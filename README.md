# bookstore
Bookstore project (Spring Boot 2, Spring 5, Java 9)

# Settings
1) Create account on: https://affiliate-program.amazon.com/gp/advertising/api/detail/main.html
2) Fill properties: aws-config.properties

# Build
./mvnw clean install

# Run
cd bookstore-rest && ../mvnw spring-boot:run

# Usage
Open: `http://localhost:8080`

