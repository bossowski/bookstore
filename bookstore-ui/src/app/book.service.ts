import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BookService {

  constructor(private http: HttpClient) { }

  searchBooks(bookTitle: string): Observable<any> {
    return this.http.get(`/books/search?bookTitle=${bookTitle}`);
  }

  getBook(id: string): Observable<any> {
    const url = `/books/${id}`;
    return this.http.get(url);
  }

  getUrlWithLowestBookPrice(bookIsbn: string): Observable<any> {
    const url = `/books/${bookIsbn}/prices`;
    return this.http.get(url);
  }
}
