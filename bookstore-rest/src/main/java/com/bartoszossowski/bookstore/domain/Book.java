package com.bartoszossowski.bookstore.domain;

import java.util.List;
import java.util.Optional;

import com.bartoszossowski.bookstore.service.booktitlesearch.google.GoogleBookServiceResponse;

/**
 * The type Book.
 */
public class Book {

    private static final String ISBN_13 = "ISBN_13";

    private final String id;
    private final String title;
    private final String subtitle;
    private final List<String> authors;
    private final String publisher;
    private final String publishedDate;
    private final String description;
    private final Optional<String> isbn;
    private final int pageCount;
    private final List<String> categories;
    private final Optional<String> thumbnailUrl;

    /**
     * Instantiates a new Book.
     *
     * @param item the item
     */
    public Book(GoogleBookServiceResponse.Item item) {
        this.id = item.getId();
        GoogleBookServiceResponse.Item.VolumeInfo volumeInfo = item.getVolumeInfo();
        this.title = volumeInfo.getTitle();
        this.subtitle = volumeInfo.getSubtitle();
        this.authors = volumeInfo.getAuthors();
        this.publisher = volumeInfo.getPublisher();
        this.publishedDate = volumeInfo.getPublishedDate();
        this.description = volumeInfo.getDescription();
        this.isbn = getIsbn13(volumeInfo.getIndustryIdentifiers());
        this.pageCount = volumeInfo.getPageCount();
        this.categories = volumeInfo.getCategories();
        this.thumbnailUrl = Optional.ofNullable(volumeInfo.getImageLinks())
            .map(GoogleBookServiceResponse.Item.VolumeInfo.ImageLinks::getThumbnail);
    }

    public String getId() {
        return id;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets subtitle.
     *
     * @return the subtitle
     */
    public String getSubtitle() {
        return subtitle;
    }

    /**
     * Gets authors.
     *
     * @return the authors
     */
    public List<String> getAuthors() {
        return authors;
    }

    /**
     * Gets publisher.
     *
     * @return the publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Gets published date.
     *
     * @return the published date
     */
    public String getPublishedDate() {
        return publishedDate;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets isbn.
     *
     * @return the isbn
     */
    public Optional<String> getIsbn() {
        return isbn;
    }

    /**
     * Gets page count.
     *
     * @return the page count
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * Gets categories.
     *
     * @return the categories
     */
    public List<String> getCategories() {
        return categories;
    }

    /**
     * Gets thumbnail url.
     *
     * @return the thumbnail url
     */
    public Optional<String> getThumbnailUrl() {
        return thumbnailUrl;
    }

    private Optional<String> getIsbn13(List<GoogleBookServiceResponse.Item.VolumeInfo.IndustryIdentifier>
                                           industryIdentifiers) {
        return Optional.ofNullable(industryIdentifiers)
            .flatMap(industryIdentifiers_ -> industryIdentifiers_.stream()
                .filter(industryIdentifier -> industryIdentifier.getType().equals(ISBN_13))
                .map(GoogleBookServiceResponse.Item.VolumeInfo.IndustryIdentifier::getIdentifier)
                .findFirst()
            );
    }
}
