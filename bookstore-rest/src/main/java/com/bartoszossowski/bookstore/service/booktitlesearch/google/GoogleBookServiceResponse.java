/*
 * Copyright © 2017 Shodann Ltd.
 * http://shodann.com/
 * All rights reserved.
 */

package com.bartoszossowski.bookstore.service.booktitlesearch.google;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * The type Google book service response.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public final class GoogleBookServiceResponse {
    private int totalItems;
    private List<Item> items;

    /**
     * Sets total items.
     *
     * @param totalItems the total items
     */
    @JsonSetter
    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    /**
     * Sets items.
     *
     * @param items the items
     */
    @JsonSetter
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * Gets total items.
     *
     * @return the total items
     */
    public int getTotalItems() {
        return totalItems;
    }

    /**
     * Gets items.
     *
     * @return the items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     * The type Item.
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static final class Item {
        private String id;
        private VolumeInfo volumeInfo;

        /**
         * Sets id.
         *
         * @param id the id
         */
        @JsonSetter
        public void setId(String id) {
            this.id = id;
        }

        /**
         * Sets volume info.
         *
         * @param volumeInfo the volume info
         */
        @JsonSetter
        public void setVolumeInfo(VolumeInfo volumeInfo) {
            this.volumeInfo = volumeInfo;
        }

        /**
         * Gets id.
         *
         * @return the id
         */
        public String getId() {
            return id;
        }

        /**
         * Gets volume info.
         *
         * @return the volume info
         */
        public VolumeInfo getVolumeInfo() {
            return volumeInfo;
        }

        /**
         * The type Volume info.
         */
        @JsonIgnoreProperties(ignoreUnknown = true)
        public static final class VolumeInfo {
            private String title;
            private String subtitle;
            private List<String> authors;
            private String publisher;
            private String publishedDate;
            private String description;
            private List<IndustryIdentifier> industryIdentifiers;
            private int pageCount;
            private List<String> categories;
            private ImageLinks imageLinks;

            /**
             * Sets title.
             *
             * @param title the title
             */
            @JsonSetter
            public void setTitle(String title) {
                this.title = title;
            }

            /**
             * Sets subtitle.
             *
             * @param subtitle the subtitle
             */
            @JsonSetter
            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            /**
             * Sets authors.
             *
             * @param authors the authors
             */
            @JsonSetter
            public void setAuthors(List<String> authors) {
                this.authors = authors;
            }

            /**
             * Sets publisher.
             *
             * @param publisher the publisher
             */
            @JsonSetter
            public void setPublisher(String publisher) {
                this.publisher = publisher;
            }

            /**
             * Sets published date.
             *
             * @param publishedDate the published date
             */
            @JsonSetter
            public void setPublishedDate(String publishedDate) {
                this.publishedDate = publishedDate;
            }

            /**
             * Sets description.
             *
             * @param description the description
             */
            @JsonSetter
            public void setDescription(String description) {
                this.description = description;
            }

            /**
             * Sets industry identifiers.
             *
             * @param industryIdentifiers the industry identifiers
             */
            @JsonSetter
            public void setIndustryIdentifiers(List<IndustryIdentifier> industryIdentifiers) {
                this.industryIdentifiers = industryIdentifiers;
            }

            /**
             * Sets page count.
             *
             * @param pageCount the page count
             */
            @JsonSetter
            public void setPageCount(int pageCount) {
                this.pageCount = pageCount;
            }

            /**
             * Sets categories.
             *
             * @param categories the categories
             */
            @JsonSetter
            public void setCategories(List<String> categories) {
                this.categories = categories;
            }

            /**
             * Sets image links.
             *
             * @param imageLinks the image links
             */
            @JsonSetter
            public void setImageLinks(ImageLinks imageLinks) {
                this.imageLinks = imageLinks;
            }

            /**
             * Gets title.
             *
             * @return the title
             */
            public String getTitle() {
                return title;
            }

            /**
             * Gets subtitle.
             *
             * @return the subtitle
             */
            public String getSubtitle() {
                return subtitle;
            }

            /**
             * Gets authors.
             *
             * @return the authors
             */
            public List<String> getAuthors() {
                return authors;
            }

            /**
             * Gets publisher.
             *
             * @return the publisher
             */
            public String getPublisher() {
                return publisher;
            }

            /**
             * Gets published date.
             *
             * @return the published date
             */
            public String getPublishedDate() {
                return publishedDate;
            }

            /**
             * Gets description.
             *
             * @return the description
             */
            public String getDescription() {
                return description;
            }

            /**
             * Gets industry identifiers.
             *
             * @return the industry identifiers
             */
            public List<IndustryIdentifier> getIndustryIdentifiers() {
                return industryIdentifiers;
            }

            /**
             * Gets page count.
             *
             * @return the page count
             */
            public int getPageCount() {
                return pageCount;
            }

            /**
             * Gets categories.
             *
             * @return the categories
             */
            public List<String> getCategories() {
                return categories;
            }

            /**
             * Gets image links.
             *
             * @return the image links
             */
            public ImageLinks getImageLinks() {
                return imageLinks;
            }

            /**
             * The type Industry identifier.
             */
            @JsonIgnoreProperties(ignoreUnknown = true)
            public static final class IndustryIdentifier {
                private String type;
                private String identifier;

                /**
                 * Sets type.
                 *
                 * @param type the type
                 */
                @JsonSetter
                public void setType(String type) {
                    this.type = type;
                }

                /**
                 * Sets identifier.
                 *
                 * @param identifier the identifier
                 */
                @JsonSetter
                public void setIdentifier(String identifier) {
                    this.identifier = identifier;
                }

                /**
                 * Gets type.
                 *
                 * @return the type
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Gets identifier.
                 *
                 * @return the identifier
                 */
                public String getIdentifier() {
                    return identifier;
                }
            }

            /**
             * The type Image links.
             */
            public static final class ImageLinks {
                private String smallThumbnail;
                private String thumbnail;

                /**
                 * Sets small thumbnail.
                 *
                 * @param smallThumbnail the small thumbnail
                 */
                @JsonSetter
                public void setSmallThumbnail(String smallThumbnail) {
                    this.smallThumbnail = smallThumbnail;
                }

                /**
                 * Sets thumbnail.
                 *
                 * @param thumbnail the thumbnail
                 */
                @JsonSetter
                public void setThumbnail(String thumbnail) {
                    this.thumbnail = thumbnail;
                }

                /**
                 * Gets small thumbnail.
                 *
                 * @return the small thumbnail
                 */
                public String getSmallThumbnail() {
                    return smallThumbnail;
                }

                /**
                 * Gets thumbnail.
                 *
                 * @return the thumbnail
                 */
                public String getThumbnail() {
                    return thumbnail;
                }
            }
        }
    }
}