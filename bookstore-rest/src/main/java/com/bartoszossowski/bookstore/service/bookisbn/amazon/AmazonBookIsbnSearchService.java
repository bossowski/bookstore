package com.bartoszossowski.bookstore.service.bookisbn.amazon;

import java.util.List;
import java.util.Optional;

import javax.xml.ws.Holder;

import com.bartoszossowski.bookstore.service.bookisbn.BookIsbnSearchService;
import com.bartoszossowski.bookstore.service.bookisbn.UrlAndPrice;
import com.bartoszossowski.bookstore.service.bookisbn.amazon.jax.AWSECommerceService;
import com.bartoszossowski.bookstore.service.bookisbn.amazon.jax.AWSECommerceServicePortType;
import com.bartoszossowski.bookstore.service.bookisbn.amazon.jax.ItemLookup;
import com.bartoszossowski.bookstore.service.bookisbn.amazon.jax.ItemLookupRequest;
import com.bartoszossowski.bookstore.service.bookisbn.amazon.jax.Items;
import com.bartoszossowski.bookstore.service.bookisbn.amazon.jax.OperationRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * The type Amazon book isbn search service.
 *
 * @see <a href="https://docs.aws.amazon.com/AWSECommerceService/latest/DG/Welcome.html">AWS Product Advertising API</a>
 */
@Service
public class AmazonBookIsbnSearchService implements BookIsbnSearchService {

    @Value("${aws.marketplace.domain}")
    private String marketplaceDomain;
    @Value("${aws.associate.tag}")
    private String associateTag;
    @Value("${aws.access.key.id}")
    private String awsAccessKeyId;
    @Value("${aws.secret.key}")
    private String awsSecretKey;

    @Override
    @Cacheable("AmazonBookIsbnSearchService")
    public Optional<UrlAndPrice> searchByIsbn(String isbn) {

        AWSECommerceService service = new AWSECommerceService();
        service.setHandlerResolver(new AwsSignatureHandlerResolver(awsSecretKey));
        AWSECommerceServicePortType port = service.getAWSECommerceServicePort();

        ItemLookupRequest request = new ItemLookupRequest();
        request.setSearchIndex("Books");
        request.setIdType("ISBN");
        request.getItemId().add(isbn);
        List<String> responseGroup = request.getResponseGroup();
        responseGroup.add("Small");
        responseGroup.add("OfferSummary");

        ItemLookup itemLookup = new ItemLookup();
        itemLookup.setMarketplaceDomain(marketplaceDomain);
        itemLookup.setAssociateTag(associateTag);
        itemLookup.setAWSAccessKeyId(awsAccessKeyId);
        itemLookup.setShared(request);
        itemLookup.setValidate("False");
        itemLookup.setXMLEscaping("Single");
        itemLookup.getRequest().add(request);

        Holder<OperationRequest> operationRequest = new Holder<>();
        Holder<List<Items>> items = new Holder<>();

        port.itemLookup(itemLookup.getMarketplaceDomain(), itemLookup.getAWSAccessKeyId(),
            itemLookup.getAssociateTag(), itemLookup.getXMLEscaping(), itemLookup.getValidate(),
            itemLookup.getShared(), itemLookup.getRequest(), operationRequest, items);

        return items.value.stream().findFirst()
            .flatMap(item_ -> item_.getItem().stream().findFirst())
            .flatMap(item -> Optional.ofNullable(item.getOfferSummary())
                .filter(offerSummary_ -> Optional.ofNullable(offerSummary_.getLowestNewPrice()).isPresent())
                .map(offerSummary_ ->
                    new UrlAndPrice(item.getDetailPageURL(), offerSummary_.getLowestNewPrice().getFormattedPrice())
                )
            );
    }


}
