package com.bartoszossowski.bookstore.service.booktitlesearch;

import com.bartoszossowski.bookstore.domain.Book;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * The interface Book title search service.
 */
public interface BookTitleSearchService {

    /**
     * Search by title flux.
     *
     * @param queryTitle the query title
     * @return the flux
     */
    Flux<Book> searchByTitle(String queryTitle);

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     */
    Mono<Book> getById(String id);
}
