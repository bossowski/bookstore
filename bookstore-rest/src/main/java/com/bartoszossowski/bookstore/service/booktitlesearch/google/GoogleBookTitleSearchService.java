package com.bartoszossowski.bookstore.service.booktitlesearch.google;

import java.util.stream.Collectors;

import com.bartoszossowski.bookstore.domain.Book;
import com.bartoszossowski.bookstore.service.booktitlesearch.BookTitleSearchService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


/**
 * The type Google book title search service.
 *
 * @see <a href="https://developers.google.com/books/">Google Books APIs</a>
 */
@Service
public class GoogleBookTitleSearchService implements BookTitleSearchService {

    /**
     * Current limit in Google Books APIs for number of results is 40.
     *
     * @see <a href="https://developers.google.com/books/docs/v1/reference/volumes/list">Google Books APIs</a>
     */
    private static final int MAX_RESULTS = 40;

    private static final String API_LIST_URL = "https://www.googleapis.com/books/v1/volumes" +
        "?q=intitle:{queryTitle}&maxResults={maxResults}";

    private static final String API_GET_URL = "https://www.googleapis.com/books/v1/volumes/{volumeId}";

    @Override
    @Cacheable("GoogleBookTitleSearchService#searchByTitle")
    public Flux<Book> searchByTitle(String queryTitle) {
        return WebClient.create().get()
            .uri(API_LIST_URL, queryTitle, MAX_RESULTS)
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlux(GoogleBookServiceResponse.class)
            .flatMap(response -> Mono.justOrEmpty(response.getItems()))
            .flatMapIterable(items -> items.stream()
                .map(Book::new)
                .filter(book -> book.getIsbn().isPresent())
                .collect(Collectors.toList()));
    }

    @Override
    @Cacheable("GoogleBookTitleSearchService#getById")
    public Mono<Book> getById(String id) {
        return WebClient.create().get()
            .uri(API_GET_URL, id)
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(GoogleBookServiceResponse.Item.class)
            .flatMap(response -> Mono.justOrEmpty(new Book(response)));
    }
}
