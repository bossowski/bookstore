package com.bartoszossowski.bookstore.service.bookisbn;

import java.util.Optional;

/**
 * The interface Book isbn search service.
 */
public interface BookIsbnSearchService {

    /**
     * Search by isbn optional.
     *
     * @param isbn the isbn
     * @return the optional
     */
    Optional<UrlAndPrice> searchByIsbn(String isbn);
}
