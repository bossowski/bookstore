package com.bartoszossowski.bookstore.service.bookisbn.apress;

import java.io.IOException;
import java.util.Optional;

import com.bartoszossowski.bookstore.service.bookisbn.BookIsbnSearchService;
import com.bartoszossowski.bookstore.service.bookisbn.UrlAndPrice;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * The type Apress book isbn search service.
 *
 * @see <a href="https://www.apress.com">Apress</a>
 */
@Service
public class ApressBookIsbnSearchService implements BookIsbnSearchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApressBookIsbnSearchService.class);

    private static final String COUNTRY_CODE = "pl";
    private static final String APRES_WEBSITE = "https://www.apress.com";

    @Override
    @Cacheable("ApressBookIsbnSearchService")
    public Optional<UrlAndPrice> searchByIsbn(String isbn) {

        Optional<String> detailsPageUrl = parseMainWebPageToGetDetailsPageUrl(isbn);
        return detailsPageUrl
            .flatMap(detailsPageUrl_ -> parseDetailsWebPageToGetPrice(detailsPageUrl_)
                .map(price -> new UrlAndPrice(detailsPageUrl.get(), price))
            );
    }

    private Optional<String> parseMainWebPageToGetDetailsPageUrl(String isbn) {
        try {
            String url = APRES_WEBSITE + "/gp/search?query=" + isbn;
            Document doc = Jsoup.connect(url)
                .cookie("springercomcountry", COUNTRY_CODE)
                .get();
            Elements detailedPage = doc.select("#result-list > div.result-item.result-item-0.result-type-book > h4 > a");
            if (!detailedPage.isEmpty()) {
                return Optional.of(APRES_WEBSITE + detailedPage.first().attr("href"));
            }
        } catch (IOException e) {
            LOGGER.warn("Error occurred while parsing main Apress website", e);
        }

        return Optional.empty();
    }

    private Optional<String> parseDetailsWebPageToGetPrice(String url) {
        try {
            Document doc = Jsoup.connect(url)
                .cookie("springercomcountry", COUNTRY_CODE)
                .get();
            Elements priceElement = doc.select("span.price-box > span");
            if (!priceElement.isEmpty()) {
                return Optional.of(priceElement.first().text().trim());
            }
        } catch (IOException e) {
            LOGGER.warn("Error occurred while parsing details Apress website", e);
        }

        return Optional.empty();
    }
}
