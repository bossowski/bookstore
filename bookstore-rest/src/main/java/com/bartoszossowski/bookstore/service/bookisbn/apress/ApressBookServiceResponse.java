/*
 * Copyright © 2017 Shodann Ltd.
 * http://shodann.com/
 * All rights reserved.
 */

package com.bartoszossowski.bookstore.service.bookisbn.apress;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * The type Apress book service response.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ApressBookServiceResponse {

    private Price price;

    /**
     * Sets price.
     *
     * @param price the price
     */
    @JsonSetter
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * The type Price.
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Price {
        private double bestPrice;

        /**
         * Sets best price.
         *
         * @param bestPrice the best price
         */
        @JsonSetter
        public void setBestPrice(double bestPrice) {
            this.bestPrice = bestPrice;
        }

        /**
         * Gets best price.
         *
         * @return the best price
         */
        public double getBestPrice() {
            return bestPrice;
        }
    }
}