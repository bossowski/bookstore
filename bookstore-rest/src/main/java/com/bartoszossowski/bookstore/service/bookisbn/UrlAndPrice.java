package com.bartoszossowski.bookstore.service.bookisbn;

import java.math.BigDecimal;

/**
 * The type Url and price.
 */
public class UrlAndPrice {

    private final String url;
    private final BigDecimal price;

    /**
     * Instantiates a new Url and price.
     *
     * @param url   the url
     * @param price the price
     */
    public UrlAndPrice(String url, String price) {
        this.url = url;
        this.price = convertToBigDecimal(price);
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    private BigDecimal convertToBigDecimal(String price) {
        return new BigDecimal(unifyDecimalPoint(removingCurrenciesSymbols(price)).trim());
    }

    private String unifyDecimalPoint(String price) {
        return price.replace(",", ".");
    }

    private String removingCurrenciesSymbols(String price) {
        return price.replaceAll("[$€]", "");
    }
}
