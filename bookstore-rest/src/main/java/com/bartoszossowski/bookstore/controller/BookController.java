package com.bartoszossowski.bookstore.controller;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.bartoszossowski.bookstore.domain.Book;
import com.bartoszossowski.bookstore.service.bookisbn.BookIsbnSearchService;
import com.bartoszossowski.bookstore.service.bookisbn.UrlAndPrice;
import com.bartoszossowski.bookstore.service.booktitlesearch.BookTitleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * The type Book controller.
 */
@RestController
@RequestMapping("/books")
public class BookController {

    private final BookTitleSearchService bookTitleSearchService;
    private final List<BookIsbnSearchService> bookIsbnSearchServices;

    /**
     * Instantiates a new Book controller.
     *
     * @param bookTitleSearchService the book title search service
     * @param bookIsbnSearchServices the book isbn search services
     */
    @Autowired
    public BookController(BookTitleSearchService bookTitleSearchService,
                          List<BookIsbnSearchService> bookIsbnSearchServices) {
        this.bookTitleSearchService = bookTitleSearchService;
        this.bookIsbnSearchServices = bookIsbnSearchServices;
    }

    /**
     * Search books response entity.
     *
     * @param bookTitle the book title
     * @return the response entity
     */
    @GetMapping("/search")
    public ResponseEntity<Flux<Book>> searchBooks(@RequestParam String bookTitle) {
        return ResponseEntity.ok(bookTitleSearchService.searchByTitle(bookTitle));
    }

    /**
     * Gets boook.
     *
     * @param id the id
     * @return the boook
     */
    @GetMapping("/{id}")
    public ResponseEntity<Mono<Book>> getBook(@PathVariable String id) {
        return ResponseEntity.ok(bookTitleSearchService.getById(id));
    }

    /**
     * Gets url with lowest book price.
     *
     * @param bookIsbn the book isbn
     * @return the url with lowest book price
     */
    @GetMapping("/{bookIsbn}/prices")
    public ResponseEntity<Optional<UrlAndPrice>> getUrlWithLowestBookPrice(@PathVariable String bookIsbn) {
        return ResponseEntity.ok(bookIsbnSearchServices.stream()
            .map(bookIsbnSearchService -> bookIsbnSearchService.searchByIsbn(bookIsbn))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .sorted(Comparator.comparing(UrlAndPrice::getPrice))
            .findFirst());
    }
}
